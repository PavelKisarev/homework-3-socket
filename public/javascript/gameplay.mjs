const texts = [
    "Text for typing #1",
    "Text for typing #2",
    "Text for typing #3",
    "Text for typing #4",
    "Text for typing #5",
    "Text for typing #6",
    "Text for typing #7",
];

const target = document.getElementById('keyb')
const progress = document.getElementById('bar')
const popup = document.getElementById('popup')
const popupCloseBtn = document.getElementById('close-popup')

target.innerHTML = texts[Math.floor(Math.random() * texts.length)]

let str = target.innerHTML
let strLength = str.length
let rng, sel

if (document.createRange) {
    rng = document.createRange();
    rng.setStart(target, 0);
    sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(rng);
} else {
    rng = document.body.createTextRange();
    rng.moveToElementText(target);
    rng.select();
}


let index = 0
let curProgress = 0
document.addEventListener('keydown', checkGameStatus)

function checkGameStatus(e) {
    if (str.charAt(index) === e.key) {
        sel.modify('extend', 'right', 'character')
        index++
    }
    curProgress = Math.floor((index / strLength) * 100)
    if (index === strLength) {

        progress.style.backgroundColor = 'green'

        let timeId = setTimeout(() => {
            popup.classList.add('show')
            clearTimeout(timeId)
            document.removeEventListener('keydown', checkGameStatus)
        }, 1000)
    }
    progress.style.width = `${curProgress}%`
    console.log('Progress = ', curProgress)
}

popupCloseBtn.addEventListener('click', () => {
    popup.classList.remove('show')
})


